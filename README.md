# Cards Against Developers #

A custom deck of [Cards Against Humanity](http://www.cardsagainsthumanity.com/).

Against developers, programmers, admins and IT folk.

Compilation of some ideas already running over the internet and brand new ones, inspired by life.

# Contribution #

Designed as a private project for me and my friends.

Feel free to add your ideas or complain over existing ones, guys. Will be nice if you check for duplicates before pushing and remove existing ones if you notice any.

## Rules ##

* Avoiding black cards with more than one space.
* The space on black cards is 3 underscores (___).

# House rules #

If someone doesn't understand a card, like a card, or think a card is funny, whether it's a black or a white card, they can discard it at any time with no penalty and replace it with a new card. This makes it easier for people from varying technical stacks and differing eras of implementation to all play together and have fun without worrying about having to prove they know some obscure trivia.

# Generating the PDF and printing #

I recommend [Bigger Blacker Cards](http://biggerblackercards.com) ([Github](https://github.com/bbcards/bbcards)). Or go for your own implementation if you wish.

The 2"x2" option on that page is the size suggested on CAH official PDF, the 2.5"x3.5" size is standard cards size.

Use thick paper, for better user experience print plain black on the back of the black cards. If you're printing in a print shop, ask them for the cutting. They'll do it faster and better.

# License #

From the CAH website: "Cards Against Humanity is available under a Creative Commons BY-NC-SA 2.0 license. That means you can use and remix the game for free, but you can�t sell it. Please do not steal our name or we will smash you."