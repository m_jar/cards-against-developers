:(){ :|:& };:
===== operator.
0.
0xDEADBEEF.
10 years since last update.
1024.
1048576 GB of RAM.
12 factor authentication.
127 little bugs in the code.
127.0.0.1.
16th coffee this day.
16-year-old #TODO.
3.5 Gb of dependency code.
404: Not found.
"413: Entity too large" errors.
418: I'm a teapot.
42.
A 2 kilometer long stack trace.
A 2000 line switch case.
A 50k line Bash script that's achieved self awareness.
A client with no servers.
A cubicle full of introverts.
A firewall full of holes.
A highly sophisticated regular expression.
A jailbroken iPhone.
A new ultrabook.
A noose made out of cat 5 cable.
A parent killing its children.
A peer that doesn't believe in peer review.
A poorly abbreviated function name.
A pseudorandom number generator.
A Python implementation that's basically just a whole bunch of C Preprocessor macros.
A random number generator that always returns 4.
A server with no client.
A version of Eclipse that doesn't crash.
Actually reading the manual.
Admin.
Adobe.
Agile.
Angry memory management in C.
Angular.js.
Another layer of abstraction.
Another Wordpress vulnerability.
Apple.
Arch Linux.
Arguing over Java or C#.
Arguing over Java or C++.
Arguing over Vim or Emacs.
Artificial stupidity.
ASCII art.
Asking a programmer to fix a hardware issue.
Backwards compatibility.
Base-17.
Bash.
Being acquired by Oracle.
Being marked CLOSED WONTFIX.
Best Practices.
Beta testing my best friend's crappy app.
Big Data.
Binaries.
Bitcoin mining botnet.
Bitcoins.
Blue Screen of Death.
Bogosort.
Brainfuck.
Bubble sort.
C++.
C++ templates compilation errors.
Changing stuff and seeing what happens.
COBOL.
Code good enough to ship.
Code I wrote 6 months ago.
Coding GUI.
Coding late at night.
Commenting every line of code.
Complete lack of common sense.
Concurrent Javascript.
Concurrent programming.
Confusing method overloading with method overriding.
Continuous disappointment.
Converting the Ikea Lack table into a 19-inch rack.
Copy paste.
Copying code from Stack Overflow that works but you don't know how.
Copy-paste errors.
Crappy code.
Cron jobs that edit cron jobs.
Cross-browser compatibility.
CSS nightmare.
Ctrl + alt + del.
Customer support.
CV.
Database antipatterns.
Date formats other than yyyy-mm-dd.
DDoSing 127.0.0.1.
Deadline driven development.
Deadlines.
Debugging 500 lines of code per method. Average.
Debugging on production.
Debugging with print statements.
Defcon.
Deleting swear words from git commits.
Deleting system32 in 64-bit Windows.
Design antipatterns.
Developers. Developers. Developers.
/dev/null
/dev/random
Documentation.
Downtime.
Eclipse.
Edward Snowden.
Error.
Error establishing connection.
Errors which disappear in debug builds.
Escaping from Vim.
Estimating time to finish the project.
Ethernet.
Excessively long variable names.
Exchanging childhood memories for 160GiB of storage.
Excuses for not writing documentation.
Extremely casual use of sudo.
Facebook.
FAT32.
Faulty code completions.
Fiat 1080p.
Fixing compilation errors by adding a comment.
Fixing grandma's printer.
FizzBuzz.
Floating-point errors.
Forgetting a semicolon.
Fork.
Former employees who still have admin access to production servers.
Functional programming.
Game of Throws.
Gender neutral programming languages.
Genetic algorithms.
`git commit -a` `git push -f`
Git merge conflicts.
Github.
GitHub selfies.
Global variables.
Google.
Googling the error messages.
goto statements.
Haskell.
haveToMakeMyOwnArrayIsEmptyMethodBecauseCrappyJavaDoesNotHaveIt();
Having to write System.out.print every time you want to print something to the screen.
Hitting it until it works.
Hope.
Horrible choice.
Hypertext Coffee Pot Control Protocol.
iEverything.
Ignoring .gitignore.
Ignoring warnings.
Indian developers.
Inefficient code.
Infinite loops.
Initializing your variables.
Installing Gentoo.
Internet Explorer.
Internet in a bucket.
Internet of Things.
Interns with root access.
Investing in dogecoins.
IPv6.
Java.
Javascript.
JetBrains.
Just plain text.
Kernel panic.
Left-handed Dvorak.
Legacy code that you're scared to delete.
LinkedIn.
Linux.
Little Bobby Tables.
Losing your Android project and having to reverse engineer your .apk to recover the code.
Mac OS X.
MacBook.
Malicious code.
man
Masochists.
Measuring the developers' performance in lines of code per day.
Mental sanity.
Microsoft.
Migration.
Mixed line endings.
Monads.
More bloat than GNU code.
My scrum master.
Naming variables.
NaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaNNaN Batman!
Needless dependencies.
New competing standard.
New incognito window.
Non-existent documentation.
Not admitting that I develop porn websites.
Not giving a shit about future maintainers of your code.
Not working.
NSA.
Null pointer exception.
Open space.
Opening braces on new lines.
Other people's code.
Parsing HTML using regular expressions.
Perl.
Ping.
Posting on Stack Overflow.
Pre-caffeine typos.
Pretending to know how it works.
Private members.
Programming drunk.
Pull request.
Pushing to production on Friday afternoon.
Rationalizing your awful hacks.
Realising that your code is shorter than your dick.
Rebasing the entire fucking repo.
Recursion.
Reddit.
Refactoring.
Refactoring all variable names to profanities.
Regex golf.
Relevant XKCD.
Removing failing tests.
Removing the start menu.
Replacing semicolons (;) with greek question marks (;).
Reporting bugs on IRC.
Rereading code of last night.
Responsive, mobile first, toilet paper.
Richard Stallmann.
`rm -rf` Russian roulette.
Root with empty password.
Running a server room in my basement.
Running fork() on Windows.
Running Java 6.
Running `rm -rf /*` as root.
Russian hackers.
Sadness as a Service.
Sarcastic code review comments.
Security by obscurity.
Security by optimism.
Segmentation fault.
Semantically important whitespace.
Sexually attractive hardware.
Shitty bugs.
Silk Road.
Slacking off in an excuse of compiling.
Spaces and tabs in one file.
Spaces in filepaths.
Spaces not tabs.
Spaghetti code.
SQLite.
SSH-ing into your backdoor.
Stack Overflow.
Stack Overflow sort.
Steering the Titanic with a popsicle stick.
Steve Jobs.
Storing private keys in /var/www.
Subjective C.
Symlinking /var/log to /dev/null.
Syntax errors.
Sysadmins sneaking Minecraft on the production server.
Tabs not spaces.
Technical debt.
Template metaprogramming.
Temporary solutions.
Testing on production.
That feel when Stack Overflow is offline.
That new JavaScript framework.
That one fucking line.
The 9th Layer of the OSI Model.
The cult of Mac.
The food stuck in my keyboard.
The interface between the computer and the chair.
The newest Windows update.
The previous developer.
The training budget.
The Travelling Salesman Problem.
The Unix epoch.
This.
Tons of legacy code.
Tor.
Treating warnings as errors.
Trolling the sysadmins.
Trusting user input.
Trying to center something in CSS.
Trying to start a variable with a number.
Turning it off and back on again.
Turning up to work still drunk from yesterday.
Two 32" 4K monitors.
Ubuntu.
UDP packets.
Undefined.
UNDO UNDO UNDO
Unicode errors.
Unicode variable names.
Unit tested underwear.
Unnecessary comments.
Unplugging a server and waiting to see who complains.
Unrealistic deadline promises to management.
Using 3 spaces indentation.
Using a float instead of an integer.
Using a switch for a boolean.
Using brainfuck on production.
UTF-9.
Vegan friendly init scripts.
Vim.
Virtual machines running virtual machines running virtual machines.
Virtual Reality porn.
Wearing protection during peer programming.
Web framework.
WebAssembly.
while(true);
Windows.
Windows 95.
Windows XP.
Women in tech.
Write-only code.
Writing all your code on one line.
Writing passwords on sticky notes.
Yet another hipster esoteric language.
Your private browsing history.
Zombie processes.